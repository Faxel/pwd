#!/bin/bash
clear
echo "
███╗   ███╗ █████╗ ███╗   ██╗
████╗ ████║██╔══██╗████╗  ██║
██╔████╔██║███████║██╔██╗ ██║
██║╚██╔╝██║██╔══██║██║╚██╗ ██║
██║ ╚═╝ ██║██║  ██║██║ ╚████║
╚═╝     ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝
▀▀█▀▀ █▀▀█ █▀▀█ █   █▀▀ ~ Codé par Kouadio ☪ ~
  █   █  █ █  █ █   ▀▀█
  ▀   ▀▀▀▀ ▀▀▀▀ ▀▀▀ ▀▀▀
";

 INSTALL_DIR="/usr/share/doc/YT20"

echo "[✔] Vérification des répertoires...";
if [ -d "$INSTALLER_DIR" ];
then
    echo "[◉] Un répertoire YT20 a été trouvé! Voulez-vous le remplacer? [O/n]:" ;
    read axel
    if [ $axel == "o" ] ;
    then
      sudo rm -R "$INSTALLER_DIR"
    else
        exit
    fi
fi

echo "[✔] Installation ...";
echo "";
sudo apt-get install -y python-pip
sudo pip install --upgrade youtube_dl
sudo apt-get install -y libav-tools
git clone https://github.com/Faxel19/YT20.git $INSTALLER_DIR;
echo "#!/bin/bash
python $INSTALLER_DIR/yt20.py" '${1+"$@"}' > yt20;
chmod +x yt20;
sudo cp YT20 /usr/bin/;


if [ -d "$INSTALLER_DIR/YT20" ];
then
    echo "";
    echo "[✔]Outil installé avec succès![✔]";
    echo "";
    echo "[✔]====================================================================[✔]";
    echo "[✔] ✔✔✔  Tout est fait!! Vous pouvez exécuter l'outil en tapant YT20  ✔✔✔ [✔]";
    echo "[✔]====================================================================[✔]";
    echo "";
else
    echo "[✘]Echec de l'installation![✘] ";
    exit
fi